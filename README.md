# PGSQL operations
Ansible role to launch PostgreSQL operations.

## Requirements

* Ansible >= 4

## OS

* Debian

## Role Variables

Variable name               | Variable description      | Type    | Default | Required
---                         | ---                       | ---     | ---     | ---
pgsql_default_conf_path     | pg_hba file path          | string  | yes     | yes
pgsql_dbencoding            | Database encoding         | string  | yes     | yes
pgsql_dblocales             | Database locales          | string  | yes     | yes
pgsql_user_encryption       | pg_hba user encryption    | string  | yes     | yes
pgsql_op                    | Operation type            | string  | no      | yes
secret.db.pgsql_dbname      | Database name             | string  | no      | yes
secret.db.pgsql_dbusername  | Database user name        | string  | no      | yes
secret.db.pgsql_dbuserpasswd | Database user password    | string  | no      | yes
pgsql_packages               | Packages list to install  | string  | yes     | yes
pgsql_listen_conf            | Listen addr in pgsql conf | string  | yes     | yes
pgsql_accessip               | User address in pg_hba    | string  | yes     | yes
pgsql_version                | pgsql version             | string  | no      | yes
pgsql_bckdir                 | pgsql backup dir          | string  | no      | yes

## Dependencies

* No

## Playbook Example

```
---
- name: Load PostgreSQL ops role
  hosts: all
  become: yes
  vars_files:
    - vars/main.yml
    - vars/vault.yml
  roles:
    - pgsql_ops
```

## License

GPL v3

## Links

* [PostgreSQL](https://www.postgresql.org/)
* [Ansible PosgreSQL modules](https://docs.ansible.com/ansible/latest/collections/community/postgresql/index.html)

## Author Information

* [Stephane Paillet](mailto:spaillet@ethicsys.fr)
